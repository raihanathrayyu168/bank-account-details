/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bank.account.details.service.impl;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.bank.account.details.service.BankDetailsService;
import in.ac.gpckasaragod.bank.account.details.ui.data.model.BankDetail;
import java.sql.PreparedStatement;
import java.util.List;

/**
 *
 * @author student
 */
public class BankDetailsServiceImpl extends ConnectionServiceImpl implements BankDetailsService {

    @Override
    public String saveBankDetails(String bankName, String branchName,String branchAddress, String branchPhoneNumber) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {

            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = branchPhoneNumber + "INSERT INTO BANK_DETAILS (BANK_NAME,BRANCH_NAME,BRANCH_ADDRESS,BRANCH_PHONE_NUMBER) VALUES "
                    + "('" + bankName + "','" + branchName +"','" + branchAddress + "','" + branchPhoneNumber + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Save successfully";
            }
        } catch (SQLException ex) {

            return "Save failed";
        }
    }

    @Override
    public BankDetail readBankDetails(Integer id) {
        //
        BankDetail bankDetail = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BANK_DETAILS WHERE ID = " + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
                String bankName = resultSet.getString("BANK_NAME");
                String branchName = resultSet.getString("BRANCH_NAME");
                String branchAddress = resultSet.getString("BRANCH_ADDRESS");
                String branchPhoneNumber = resultSet.getString("BRANCH_PHONE_NUMBER");
                bankDetail = new BankDetail(id, bankName,branchName, branchAddress, branchPhoneNumber);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BankDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bankDetail;
    }

    public List<BankDetail> getAllBankDetails() {
        List<BankDetail> bankDetails = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BANK_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String bankName = resultSet.getString("BANK_NAME");
                String branchName = resultSet.getString("BRANCH_NAME");
                String branchAddress = resultSet.getString("BRANCH_ADDRESS");
                String branchPhoneNumber = resultSet.getString("BRANCH_PHONE_NUMBER");
            }
        } catch (SQLException ex) {
            Logger.getLogger(BankDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bankDetails;
    }

    @Override
    public String updateBankdetails(Integer id, String bankName,String branchName, String branchAddress, String branchPhoneNumber) {
        //
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE BANK_DETAILS SET BANK_NAME='" + bankName + "',BRANCH_NAME='" + branchName +"',BRANCH_ADDRESS='" + branchAddress + "',BRANCH_PHONE_NUMBER='" + branchPhoneNumber + "' WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Update successfully";
            }
        } catch (SQLException ex) {
            return "Update failed";
        }
    }

    @Override
    public String deleteBankDetail(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM BANK_DETAILS WHERE ID +?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            }else {
                return "Delete successfully";
            }
        }catch (SQLException ex) {
            return "Delete failed";
            
            }
        }
    } 

   

