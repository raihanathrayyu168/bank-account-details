/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bank.account.details.service.impl;

import in.ac.gpckasaragod.bank.account.details.service.AccountDetailsService;
import in.ac.gpckasaragod.bank.account.details.ui.data.model.AccountDetail;
import in.ac.gpckasaragod.bank.account.details.ui.data.model.BankDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class AccountDetailsServiceImpl extends ConnectionServiceImpl implements AccountDetailsService {
    public String saveAccountDetails(String accountNumber, String consumerName,String consumerOccupation, Date openingDate, Date closingDate) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {

            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO BANK_ACCOUNT_DETAILS (ACCOUNT_NUMBER,CONSUMER_NAME,CONSUMER_OCCUPATION,OPENING_DATE,CLOSING_DATE) VALUES "
                    + "('" + accountNumber + "','" + consumerName +"','" + consumerOccupation + "','" + openingDate + "','" +  closingDate +"')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Save successfully";
            }
        } catch (SQLException ex) {

            return "Save failed";
        }
    }

    @Override
    public AccountDetail readAccountDetails(Integer id) {
        //
        AccountDetail accountDetail = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BANK_ACCOUNT_DETAILS WHERE ID = " + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
                String accountNumber = resultSet.getString("ACCOUNT_NUMBER");
                String consumerName = resultSet.getString("CONSUMER_NAME");
                String consumerOccupation = resultSet.getString("CONSUMER_OCCUPATION");
                String openingDate= resultSet.getString("OPENING_DATE");
                String closingDate= resultSet.getString("CLOSING_DATE");
                SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
                accountDetail = new AccountDetail(id, accountNumber,consumerName,consumerOccupation,sdf.parse(openingDate),sdf.parse(closingDate));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AccountDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountDetail;
    }

    public List<AccountDetail> getAllAccountDetails() {
        List<AccountDetail> accountDetails = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BANK_ACCOUNT_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String accountNumber = resultSet.getString("ACCOUNT_NUMBER");
                String consumerName = resultSet.getString("CONSUMER_NAME");
                String consumerOccupation = resultSet.getString("CONSUMER_OCCUPATION");
                String openingDate= resultSet.getString("OPENING_DATE");
                String closingDate= resultSet.getString("CLOSING_DATE");
            }
        } catch (SQLException ex) {
            Logger.getLogger(BankDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountDetails;
    }

    @Override
    public String updateAccountdetails(Integer id, String accountNumber,String consumerName, String consumerOccupation, Date openingDate, Date closingDate) {
        //
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE BANK_ACCOUNT_DETAILS SET ACCOUNT_NUMBER='" + accountNumber + "',CONSUMER_NAME='" + consumerName +"',CONSUMER_OCCUPATION='" + consumerOccupation + "',OPENING_DATE='" + openingDate + "',CLOSING_DATE='" + closingDate + "' WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Update successfully";
            }
        } catch (SQLException ex) {
            return "Update failed";
        }
    }

    @Override
    public String deleteAccountDetail(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM ACCOUNTDETAIL WHERE ID +?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            }else {
                return "Delete successfully";
            }
        }catch (SQLException ex) {
            return "Delete failed";
            
            }
        }

  
    } 

   

    

