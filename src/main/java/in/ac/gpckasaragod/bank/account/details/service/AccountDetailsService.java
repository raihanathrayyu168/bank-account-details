/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bank.account.details.service;

import in.ac.gpckasaragod.bank.account.details.ui.data.model.AccountDetail;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface AccountDetailsService {
      public String saveAccountDetails(String accountNumber, String consumerName,String consumerOccupation, Date openingDate, Date closingDate);
    public AccountDetail readAccountDetails(Integer id);
    public List<AccountDetail> getAllAccountDetails();
    public String updateAccountdetails(Integer id, String accountNumber,String consumerName, String consumerOccupation, Date openingDate, Date closingDate);
    public String deleteAccountDetail(Integer id) ;

    
}
