/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bank.account.details.service;

import in.ac.gpckasaragod.bank.account.details.ui.data.model.BankDetail;
import java.util.List;


/**
 *
 * @author student
 */
public interface BankDetailsService {

    public String deleteBankDetail(Integer id);
    public String   saveBankDetails(String bankName,String branchName,String branchAddress,String branchPhoneNumber);
    public  BankDetail readBankDetails(Integer id);
    public List< BankDetail>getAllBankDetails();
 public String updateBankdetails(Integer id,String bankName,String branchName, String branchAddress,String branchPhoneNumber) ;
}
