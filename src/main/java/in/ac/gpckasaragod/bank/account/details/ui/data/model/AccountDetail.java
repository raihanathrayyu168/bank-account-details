/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bank.account.details.ui.data.model;

import java.util.Date;

/**
 *
 * @author student
 */
public class AccountDetail {
    public Integer id;
    public String  accountNumber;
    public String  consumerName;
    public String  consumerOccupation;
    public Date  openingDate;
    public Date  closingDate;

    public AccountDetail(Integer id, String accountNumber, String consumerName, String consumerOccupation, Date openingDate, Date closingDate) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.consumerName = consumerName;
        this.consumerOccupation = consumerOccupation;
        this.openingDate = openingDate;
        this.closingDate = closingDate;
    }

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getConsumerOccupation() {
        return consumerOccupation;
    }

    public void setConsumerOccupation(String consumerOccupation) {
        this.consumerOccupation = consumerOccupation;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }
    
    
    
    
}
